#!/usr/bin/env bash

function check_if_available() {
  curl -Is "$1" | head -1 | egrep "[1-3][[:digit:]][[:digit:]]" > /dev/null || echo "$1"
}


if [[ $# -eq 0 ]]; then
  check_if_available "http://google.es" || exit 1

  exit 0
fi

which curl > /dev/null || exit 1

for var in "$@"
do
    check_if_available "$var"
done