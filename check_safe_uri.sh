#!/usr/bin/env bash

if [[ ! $APIKEY ]]; then
  echo "APIKEY environment variable not defined."
  exit 4
fi

if [[ $# -eq 0 ]]; then
  echo "http://testsafebrowsing.appspot.com/apiv4/ANY_PLATFORM/MALWARE/URL/" | sblookup -apikey=$APIKEY 2> /dev/null \
    | grep "[{testsafebrowsing.appspot.com/apiv4/ANY_PLATFORM/MALWARE/URL/ {MALWARE ANY_PLATFORM URL}}]" > /dev/null \
    || exit 3

  exit 0
fi

which sblookup > /dev/null || { echo "Error: 'sblookup' is not installed."; exit 2; }

for var in "$@"
do
    echo $var | sblookup -apikey=$APIKEY 2> /dev/null | grep Unsafe | cut -d' ' -f3-
done