#!/usr/bin/env sh

docker run -d --name url-shortener-db -p 5432:5432 registry.gitlab.com/webengproteam/url-shortener/database-schema
