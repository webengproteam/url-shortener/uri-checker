FROM golang:alpine

RUN apk update && apk add git
RUN export PATH=$PATH:$GOPATH/bin
RUN go get github.com/google/safebrowsing/cmd/sblookup

########################################################
FROM alpine:latest

RUN apk update && apk add bash curl postgresql-client
COPY --from=0 /go/bin/sblookup /bin/
COPY check* /usr/src/app/
RUN chmod u+x /usr/src/app/check*

ENTRYPOINT ["/usr/src/app/checker.sh"]
CMD ["60"]
