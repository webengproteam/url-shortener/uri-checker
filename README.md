# URI Checker

**Master:** [![pipeline status](https://gitlab.com/webengproteam/url-shortener/uri-checker/badges/master/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/uri-checker/commits/master)
**Test:** [![pipeline status](https://gitlab.com/webengproteam/url-shortener/uri-checker/badges/test/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/uri-checker/commits/test)

Checks if a URI es accessible and secure (in terms of Google Safe Browsing).