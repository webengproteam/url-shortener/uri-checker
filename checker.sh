#!/usr/bin/env bash

env_vars="DB_NAME DB_USER DB_PASS DB_SERVICE DB_PORT"

for var in $env_vars; do
  if [ -z ${!var} ]; then
    echo "$var is unset"
    exit 1
  fi
done

which psql > /dev/null || exit 1

function check_uris() {
  while read -r line
  do
      short=$(echo $line | cut -d' ' -f1)
      long=$(echo $line | cut -d' ' -f2)
      accessible=$(echo $line | cut -d' ' -f3)
      secure=$(echo $line | cut -d' ' -f4)

      echo "$short --- $long --- $accessible --- $secure"

      not_available="$(bash ./check_available_uri.sh $long)"
      not_secure="$(bash ./check_safe_uri.sh $long)"

      echo "$not_available --- $not_secure"

      if [[ $not_available ]] && [[ $accessible == "t" ]]; then

        PGPASSWORD=$DB_PASS psql "postgresql://$DB_SERVICE:$DB_PORT/$DB_NAME?sslmode=disable" \
          -U $DB_USER -c "update uri set accessible = 'f' where short_uri = '$short';"

      elif [[ $not_available == "" ]] && [[ $accessible == "f" ]]; then

        PGPASSWORD=$DB_PASS psql "postgresql://$DB_SERVICE:$DB_PORT/$DB_NAME?sslmode=disable" \
          -U $DB_USER -c "update uri set accessible = 't' where short_uri = '$short';"

      fi

      if [[ $not_secure ]] && [[ $secure == "t" ]]; then

        PGPASSWORD=$DB_PASS psql "postgresql://$DB_SERVICE:$DB_PORT/$DB_NAME?sslmode=disable" \
          -U $DB_USER -c "update uri set secure = 'f' where short_uri = '$short';"

      elif [[ $not_secure == "" ]] && [[ $secure == "f" ]]; then

        PGPASSWORD=$DB_PASS psql "postgresql://$DB_SERVICE:$DB_PORT/$DB_NAME?sslmode=disable" \
          -U $DB_USER -c "update uri set secure = 't' where short_uri = '$short';"

      fi

      echo
  done < <(PGPASSWORD=$DB_PASS psql "postgresql://$DB_SERVICE:$DB_PORT/$DB_NAME?sslmode=disable" \
    -U $DB_USER -c "select * from uri;" | head -n-2 | tail -n+3 | awk '{print $1, $3, $5, $9}')
}

if [[ $# -eq 0 ]]; then
  check_uris
elif [[ $# -eq 1 ]]; then
  re='^[0-9]+$'

  if [[ $1 =~ $re ]]; then
    while true; do
      check_uris &
      sleep "$1"
    done
  else
    echo "Please, specify a valid number of seconds."
  fi
fi